from time import time_ns
from src.weightedgrid import WeightedGrid
from src.dstarlitenaive import DStarLiteNaive

if __name__ == "__main__":
    # small (64x64)
    # grid = WeightedGrid.from_bmp_file('./map-0.bmp')

    # large (1000x1000)
    grid = WeightedGrid.from_bmp_file('./map-random-0.bmp')

    # worst case scenario, a maze (100x100)
    # grid = WeightedGrid.from_bmp_file('./maze-100-0.bmp')

    algo = DStarLiteNaive(grid)
    start_point = (4, 60)
    end_point = (600, 400)

    timer_start = time_ns()
    result = algo.compute(start_point, end_point)
    timer_end = time_ns()
    diff = timer_end - timer_start

    print(result)
    print(f'Path {"NOT " if len(result) == 0 else ""}found')
    print(f'Search took {diff / 1000000} ms')

    # sleep(10)
