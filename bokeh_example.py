from time import time_ns
from bokeh.server.server import Server
from bokeh.application.application import Application
from bokeh.application.handlers import FunctionHandler
from bokeh.document import Document
from src.weightedgrid import WeightedGrid
from src.dstarlitenaive import DStarLiteNaive


def make_document(doc: Document):
    # Warning! DO NOT run large maps with visualization. It's extremely
    # inefficient right now and will make your PC cry.
    grid = WeightedGrid.from_bmp_file('./examples/map-0.bmp')
    algo = DStarLiteNaive(grid, True)
    start_point = (40, 60)
    end_point = (60, 4)

    timer_start = time_ns()
    result = algo.compute(start_point, end_point)
    timer_end = time_ns()
    diff = timer_end - timer_start

    print(result)
    print(f'Path {"NOT " if len(result) == 0 else ""}found')
    print(f'Search took {diff / 1000000} ms')

    doc.title = "Algo test"
    vis = algo.get_visualization();
    if (vis is not None):
        doc.add_root(vis)


# Start on webpage load
apps = {'/': Application(FunctionHandler(make_document))}

server = Server(apps, port=5006)
print(
    f'To see the results of the algorithm go to: http://localhost:{server.port}')

# Makes the server run indefinitely.
# In case of error, before the next restart, make sure the python process is closed.
server.run_until_shutdown()
