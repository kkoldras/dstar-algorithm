# D* Lite shortest path algorithm

## Installation

Project uses [PDM](https://pdm.fming.dev/latest/) as a package manager. 
Installation instructions can be found on [PDM's website](https://pdm.fming.dev/latest/#installation).

Once PDM is install clone the git repository and enter newly created directory:
```sh
git clone https://gitlab.com/kkoldras/dstar-algorithm.git
cd dstar-algorithm
```

After that run following command to install all dependencies (when used with 
default configuration, this should also create a new venv):
```sh
pdm install
```

Next to start the application run:
```sh
pdm start
```
It exectutes a start script defined in `pyproject.toml`.
After a short while the application will be available under the address 
http://localhost:5006.