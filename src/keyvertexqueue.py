import math
from typing import Tuple, List
from src.algorithm import Vertex

Key = Tuple[float, float]


class QueueElement:
    """
    Defines a single element of the KeyVertexQueue.
    """

    def __init__(self, key: Key, vertex: Vertex):
        """
        Default constructor.

        :param key: a key is a tuple with 2 keys used to order the queue.
        :param vartex: a tuple containing x and y coordinates of the vertex
        """
        self.key = key
        self.vertex = vertex


class KeyVertexQueue:
    """
    A queue of vertices ordered by 2-element keys. This implementation
    completely omits sorting, relaying on inserting an element at the correct
    place immediately.
    Note: both keys and vertices are expected to be tuples with 2 elements -
    x, y values for vertex and k1, k2 values for key.
    """

    queue: List[QueueElement] = []

    def top(self) -> QueueElement:
        """
        Returns a queue element with the highest priority / lowest key / first
        in queue. Does NOT remove the element from the queue.
        """
        return self.queue[0]

    def top_vertex(self) -> Vertex:
        """
        Returns a vertex with the highest priority / lowest key / first in
        queue.
        """
        return self.top().vertex

    def top_key(self) -> Key:
        """
        Returns a key with the highest priority / lowest value.
        """
        if len(self.queue) == 0:
            return (math.inf, math.inf)
        return self.top().key

    def pop(self) -> QueueElement:
        """
        Returns a queue element with the highest priority / lowest key / first
        in queue. Removes the element from the queue.
        """
        return self.queue.pop(0)

    def insert(self, vertex: Vertex, key: Key) -> None:
        """
        Inserts a new vertex into the queue, ordered by the provided key.

        :param vertex: vertex to add.
        :param key: key of the vertex.
        """
        self.queue.insert(self.next_index(key), QueueElement(key, vertex))

    def remove(self, vertex: Vertex) -> None:
        """
        Removes a vertex from the queue. If the queue doesn't contain it,
        nothing happens.

        :param vertex: vertex to remove.
        """
        idx = -1
        for enqueued in self.queue:
            if enqueued.vertex == vertex:
                idx = self.queue.index(enqueued)
                break
        if idx > -1:
            self.queue.pop(idx)

    def contains(self, vertex: Vertex) -> bool:
        """
        Checks wether the queue contains given vertex.

        :param vertex: vertex to check.
        :return: true if the vertex is in the queue, otherwise false
        """
        for enqueued in self.queue:
            if enqueued.vertex == vertex:
                return True
        return False

    def update(self, vertex: Vertex, key: Key) -> None:
        """
        Update vertex's position in the queue with a new key.

        :param vertex: vertex to update.
        :param key: new key.
        """
        self.remove(vertex)
        self.insert(vertex, key)

    def next_index(self, key: Key) -> int:
        """
        Calculates an index in the queue based on the key.

        :param key: key
        :return: position in the queue of the provided key.
        """
        if len(self.queue) == 0:
            return 0
        elements_before_or_equal_key0 = [
            e for e in self.queue if e.key[0] <= key[0]]
        max_len = len(elements_before_or_equal_key0)

        if max_len == 0:
            return max_len
        if max_len == len(self.queue):
            return max_len
        if key[0] != elements_before_or_equal_key0[-1].key[0]:
            return max_len
        else:
            elements_after_key = 0
            for i in range(max_len - 1, -1, -1):
                elem = elements_before_or_equal_key0[i]
                if elem.key[0] == key[0] and elem.key[1] > key[1]:
                    elements_after_key += 1
                else:
                    break
            return max_len - elements_after_key
