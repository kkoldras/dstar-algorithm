from typing import List, Tuple
from abc import ABC, abstractmethod
from bokeh.models.layouts import Column
from src.weightedgrid import WeightedGrid

Vertex = Tuple[int, int]


class Algorithm(ABC):
    """
    Represents an algorithm used in pathfinding. Provides two functionalities
    that should be called in order:
    1. compute - Computes the whole path once. This returns a list of vertices
        that a drone can follow. If the map cost doesn't change this is the
        final result. This has to be called only once, each time an end point
        changes.
    2. change_occurred - Any time a new obstacle is found, a recalculation
        should occur. This notifies the algorithm, that grid contains new data
        and that the drone position changed. It will return a new list of
        vertices from the provided new point to the end. Important: if the end
        point changes compute should be called again instead of this.
    """

    def __init__(self, grid: WeightedGrid, visualize: bool = False) -> None:
        """
        Initialize the algorithm instance.

        :param grid: grid representing the map.
        :param visualize: weather to enable collection of additional data for
            the visualization.
        """
        self.grid = grid
        self.visualize = visualize

    @abstractmethod
    def compute(self, start: Vertex, end: Vertex) -> List[Vertex]:
        """
        Initializes an algorithm and returns a path from provided start to the
        end.


        :param start: tuple with start coordinates (x, y)
        :param end: tuple with goal coordinates (x, y)
        :return: list of points that needs to be followed in order to traverse
            between start and end. If such a route cannot be found the, returns
            empty list.
        """
        raise NotImplementedError

    @abstractmethod
    def change_occurred(self, new_position: Vertex) -> List[Vertex]:
        """
        Notifies algorithm that some changes happened to the grid and a new path
        is needed.

        :param new_position: position from which the new path needs to be found.
            It has to be one of the values returned by compute or previous
            change_occurred call.
        :return: list of points that needs to be followed in order to traverse
            between new_position and end. If such a route cannot be found the
            method, returns an empty list.
        """
        raise NotImplementedError

    @abstractmethod
    def get_visualization(self) -> Column:
        """
        Get bokeh figure, that will be used to visualize the algorithm steps.

        :return: Column object, that can be added to a visualization or None if
            visualization is disabled.
        """
        raise NotImplementedError
