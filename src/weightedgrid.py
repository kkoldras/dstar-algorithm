from math import inf
import numpy as np
from PIL import Image


class WeightedGrid:
    """
    Describes the grid object used for pathfinding.
    WIP.
    """

    def __init__(self, width: int, height: int):
        """
        Creates a new WeightedGrid. All cells are initialized to 0.

        :param width: width of the grid.
        :param height: height of the grid.
        """
        self.size = (width, height)
        self.array: np.ndarray = np.zeros((width, height))

    def apply_weights(self, weights: np.ndarray):
        """
        Apply a weights to the existing grid.

        :param weights: ndarray containing a weight for each cell. Weights value
            will be multiplied with cell's value.
        """
        self.array *= weights

    @staticmethod
    def from_bmp_file(map_file: str):
        """
        Creates a grid from a BMP file. Values from each channel (R, G, B) are
        added together. White pixels (#FFFFFF) are converted to infinity
        (math.inf).

        :param map_file: path to the image.
        :return: WeightedGrid object.
        """
        img: Image.Image = Image.open(map_file)
        pixels = np.array(img, dtype=float)
        values = np.sum(pixels, axis=2)
        values[values == 255 + 255 + 255] = inf

        grid = WeightedGrid(*values.shape)
        # Changes the image from image coordinates (y=0 is top) to mathematical
        # coordinates (y=0 is bottom)
        grid.array = np.flipud(values)
        return grid
