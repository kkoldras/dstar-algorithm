import math
from typing import List, Optional
import numpy as np
from src.weightedgrid import WeightedGrid
from src.algorithm import Algorithm, Vertex
from src.keyvertexqueue import KeyVertexQueue, Key


class LPAStar(Algorithm):
    """
    Algorithm used for comparison with D* Lite, as first version D* Lite is
    partially based on LPA*.
    """

    _SR2 = math.sqrt(2)

    def __init__(self, grid: WeightedGrid):
        super().__init__(grid)

        self.u_queue: KeyVertexQueue
        self.s_start: Vertex
        self.s_goal: Vertex
        self.s_grid: np.ndarray = grid.array
        self.s_last: Vertex
        self.g_values: np.ndarray
        self.rhs_values: np.ndarray

    def compute(self, grid: WeightedGrid, start: Vertex, end: Vertex) -> List[Vertex]:
        self.u_queue = KeyVertexQueue()  # priority queue

        self.s_start = start
        self.s_goal = end

        self.g_values = np.full(grid.size, math.inf)
        self.rhs_values = np.full(grid.size, math.inf)

        self.rhs_values[self.s_start[1], self.s_start[0]] = 0

        self.u_queue.insert(self.s_start, self.__calc_key(self.s_start))
        self.__compute_shortest_path()
        result = self.__get_shortest_path()

        return result

    def change_occurred(self, new_position: Vertex) -> List[Vertex]:
        """
        Not implemented.
        """
        # while True:
        #     compute_shortest_path
        #     wait for changes in edge costs
        #     for all directed edges (u, v) with changed edge costs:
        #         update the edge cost c(u, v)
        #         update_vertex(u)

    def __update_vertex(self, u_vertex: Vertex) -> None:
        if u_vertex != self.s_start:
            self.rhs_values[u_vertex[1], u_vertex[0]] = self.__calc_rhs(u_vertex)
        self.u_queue.remove(u_vertex)
        if self.__get_g(u_vertex) != self.__get_rhs(u_vertex):
            self.u_queue.insert(u_vertex, self.__calc_key(u_vertex))

    def __compute_shortest_path(self) -> None:
        while self.u_queue.top_key() < self.__calc_key(self.s_goal) or self.__get_rhs(
            self.s_goal
        ) != self.__get_g(self.s_goal):
            u_vertex = self.u_queue.pop().vertex
            if self.__get_g(u_vertex) > self.__get_rhs(u_vertex):
                self.g_values[u_vertex[1], u_vertex[0]
                              ] = self.__get_rhs(u_vertex)
                for s_vertex in self.__successors(u_vertex):
                    self.__update_vertex(s_vertex)
            else:
                self.g_values[u_vertex[1], u_vertex[0]] = math.inf
                for s_vertex in self.__successors(u_vertex):
                    self.__update_vertex(s_vertex)
                self.__update_vertex(u_vertex)

    # edge cost
    def __c_cost(self, s_vertex: Vertex, s_succ: Vertex) -> float:
        if s_vertex == s_succ:
            return 0
        if s_vertex[0] != s_succ[0] and s_vertex[1] != s_succ[1]:
            return self._SR2
        else:
            return 1

    def __neighbors(self, s_vertex: Vertex) -> List[Vertex]:
        # clockwise order
        result = []
        for i in range(-1, 2):
            for j in range(-1, 2):
                if i == j == 0:
                    continue
                x = s_vertex[0] + i
                y = s_vertex[1] + j
                if (
                    0 <= x < self.s_grid.shape[0]
                    and 0 <= y < self.s_grid.shape[1]
                    and self.s_grid[y, x] != math.inf
                ):
                    result.append((x, y))
        return result

    def __successors(self, s_vertex: Vertex) -> List[Vertex]:
        return self.__neighbors(s_vertex)
        # neighbors = self.neighbors(s)
        # return [e for e in neighbors if e not in self.predecessors]

    def __predecessors(self, s_vertex: Vertex) -> List[Vertex]:
        return self.__neighbors(s_vertex)

    def __calc_rhs(self, s_vertex: Vertex) -> float:
        values = [self.__get_g(s_prim) + self.__c_cost(s_prim, s_vertex)
                  for s_prim in self.__predecessors(s_vertex)]
        return min(values)

    def __get_rhs(self, s_vertex: Vertex):
        return self.rhs_values[s_vertex[1], s_vertex[0]]

    def __get_g(self, s_vertex: Vertex):
        return self.g_values[s_vertex[1], s_vertex[0]]

    # heuristic - octile distance
    # h(s_goal, s_goal) == 0
    # h(s, s_goal) <= c(s, s_succ) + h(s_succ, s_goal)
    def __get_h(self, s_vertex: Vertex, s_prim: Vertex) -> float:
        if s_vertex == s_prim:
            return 0
        d_x = abs(s_vertex[0] - s_prim[0])
        d_y = abs(s_vertex[1] - s_prim[1])
        return (d_x + d_y) + (self._SR2 - 2) * min(d_x, d_y)

    # k(s) = [k1(s); k2(s)]
    # k1(s) = min(g(s), rhs(s)) + h(s, s_goal)
    # k2(s) = min(g(s), rhs(s))
    def __calc_key(self, s_vertex: Vertex) -> Key:
        key1 = min(self.__get_g(s_vertex), self.__get_rhs(s_vertex)) + \
            self.__get_h(s_vertex, self.s_goal)
        key2 = min(self.__get_g(s_vertex), self.__get_rhs(s_vertex))
        return (key1, key2)

    def __get_shortest_path(self) -> List[Vertex]:
        path = []
        if self.__get_g(self.s_goal) != math.inf:
            s_vertex = self.s_goal
            while s_vertex != self.s_start:
                path.append(s_vertex)
                # for each predecessor calculate g(p) + c(p, s) and select one
                # with lowest result
                _, s_vertex = min(
                    [(self.__get_g(p) + self.__c_cost(p, s_vertex), p)
                     for p in self.__predecessors(s_vertex)]
                )
            path.append(self.s_start)
            path.reverse()
        return path
