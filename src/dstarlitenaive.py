from typing import List, Optional
import math
from bokeh.colors import RGB
from bokeh.layouts import layout
from bokeh.models import Column, ColumnDataSource, Div, Slider, HoverTool, RadioButtonGroup
from bokeh.plotting import figure
from bokeh.events import Tap, Event

import numpy as np
from src.weightedgrid import WeightedGrid
from src.algorithm import Algorithm, Vertex
from src.keyvertexqueue import KeyVertexQueue, Key


class DStarLiteNaive(Algorithm):
    """
    A first optimized implementation of the D* Lite algorithm. Still not
    completely finished (lacks implementation for dynamic path updates),
    still can be optimized. It's called naive, because it
    closely follows pseudocode from the original paper. That's the reason the
    variables are so nondescriptive - they closely match what is in the paper
    so it's easier to track and understand.

    Based entirely on this paper:
    https://www.cs.cmu.edu/~maxim/files/dlite_tro05.pdf

    Summary of notation used in this implementation:
    - u_queue - Vertex queue used in calculation, prioritized by key with
        2 values: key1 and key2.
    - s_start - starting point on the grid.
    - s_goal - end point to which the algorithm is trying to find a path.
    - s_grid - grid containing all points and their weights.
    - s_last - previous position on a found path. Used for dynamic updates.
    - g_values - grid containing calculated g-values for current path search for
        each point on the S grid. Starts with all points set to infinity. Each
        point is updated only when "visited" by the algorithm.
    - rhs_values - grid containing calculated rhs-values, works the same way as
        g_values.
    - km - key modifier, used in dynamic path changes.

    - g-value - is the cost of the path from the start to a given node.
    - rhs-value - so called right-hand side value, rhs-value of a vertex is
        based on the g-values of its predecessors
    - h-value - value of a heuristic function that estimates the cost of the
        cheapest path from a given node to the goal.
    - c-value - cost of moving along a given graph edge.

    TODO:
    - implement proper cost calculation - c_cost() method needs return different
        values based on map weights.
    - implement dynamic updates - convert pseudocode into change_occurred()
        method.
    """

    _SR2 = math.sqrt(2)
    km = 0

    def __init__(self, grid: WeightedGrid, visualize: bool = False):
        super().__init__(grid, visualize)

        self.u_queue: KeyVertexQueue
        self.s_start: Vertex
        self.s_goal: Vertex

        self.s_grid: np.ndarray = grid.array
        self.s_last: Vertex
        self.g_values: np.ndarray
        self.rhs_values: np.ndarray

        if visualize:
            self.g_values_cache: List[np.ndarray] = []
            self.rhs_values_cache: List[np.ndarray] = []
            self.result: List[Vertex] = []
            self.edit_mode = None
            self.visual_result: List = []

    def compute(self, start: Vertex, end: Vertex) -> List[Vertex]:
        # Vertex priority queue
        self.u_queue = KeyVertexQueue()

        self.s_start = start
        self.s_goal = end

        self.s_last = self.s_start
        self.g_values = np.full(self.s_grid.shape, math.inf)
        self.rhs_values = np.full(self.s_grid.shape, math.inf)
        if self.visualize:
            self.g_values_cache.append(self.g_values.copy())
            self.rhs_values_cache.append(self.rhs_values.copy())

        self.rhs_values[self.s_goal[1], self.s_goal[0]] = 0

        self.u_queue.insert(
            self.s_goal, (self.__get_h(self.s_start, self.s_goal), 0))
        self.__compute_shortest_path()
        result = self.__get_shortest_path()
        if self.visualize:
            self.result = result
            self.visual_result = get_visual_result(result)

        return result

    def change_occurred(self, new_position: Vertex) -> List[Vertex]:
        """
        Not implemented. Yet.
        """
        # while self.s_start != self.s_goal:
        #     if self.rhs(self.s_start) == math.inf:
        #         # there is no path
        #         break
        #     # argmin
        #     _, am = min(
        #       [(self.g(p) + self.c(self.s_start, p), p)
        #        for p in self.successors(self.s_start)])
        #     self.s_start = am

        #     move to s_start
        #     scan map for changes
        #     if edge cost changed:
        #         self.km += h(self.s_last, self.s_start)
        #         self.s_last = self.s_start
        #         for all directed edges (u, v) with changed edge costs:
        #             c_old = self.c(u, v)
        #             update the edge cost c(u, v)
        #             if c_old > self.c(u, v):
        #                 self.rhs_values[u[1], u[0]] = min(self.rhs(u), self.c(u, v) + self.g(v))
        #             elif u != self.s_goal and self.rhs(u) == c_old + self.g(v):
        #                 self.rhs_values[u[1], u[0]] = min(
        #                   [self.c(u, p) + self.g(p)
        #                    for p in self.successors(u)])
        #             update_vertex(u)
        #         compute_shortest_path()
        return self.result

    def __update_vertex(self, u_vertex: Vertex) -> None:
        """
        Update information about the vertex.
        """
        g_eq_rhs = self.__get_g(u_vertex) == self.__get_rhs(u_vertex)
        u_in_queue = self.u_queue.contains(u_vertex)
        if not g_eq_rhs and u_in_queue:
            self.u_queue.update(u_vertex, self.__calc_key(u_vertex))
        elif not g_eq_rhs and not u_in_queue:
            self.u_queue.insert(u_vertex, self.__calc_key(u_vertex))
        elif g_eq_rhs and u_in_queue:
            self.u_queue.remove(u_vertex)

    def __compute_shortest_path(self) -> None:
        """
        Perform calculations necessary for finding the path.
        """
        while self.u_queue.top_key() < self.__calc_key(self.s_start) or self.__get_rhs(
            self.s_start
        ) > self.__get_g(self.s_start):
            top = self.u_queue.top()
            u_vertex = top.vertex
            k_old = top.key
            k_new = self.__calc_key(u_vertex)

            if k_old < k_new:
                self.u_queue.update(u_vertex, k_new)
            elif self.__get_g(u_vertex) > self.__get_rhs(u_vertex):
                self.g_values[u_vertex[1], u_vertex[0]
                              ] = self.__get_rhs(u_vertex)
                self.u_queue.remove(u_vertex)
                for s_vertex in self.__predecessors(u_vertex):
                    self.rhs_values[s_vertex[1], s_vertex[0]] = min(
                        self.__get_rhs(s_vertex), self.__c_cost(
                            s_vertex, u_vertex) + self.__get_g(u_vertex)
                    )
                    self.__update_vertex(s_vertex)
            else:
                g_old = self.__get_g(u_vertex)
                self.g_values[u_vertex[1], u_vertex[0]] = math.inf

                for s_vertex in [*self.__predecessors(u_vertex), u_vertex]:
                    if s_vertex != self.s_goal and \
                            self.__get_rhs(u_vertex) == self.__c_cost(s_vertex, u_vertex) + g_old:
                        self.rhs_values[s_vertex[1], s_vertex[0]] = self.__calc_rhs(s_vertex)
                    self.__update_vertex(s_vertex)

            if self.visualize:
                self.g_values_cache.append(self.g_values.copy())
                self.rhs_values_cache.append(self.rhs_values.copy())

    def __c_cost(self, s_vertex: Vertex, s_succ: Vertex) -> float:
        """
        Edge cost calculation. Still needs to include values from the map.
        """
        if s_vertex == s_succ:
            return 0
        if s_vertex[0] != s_succ[0] and s_vertex[1] != s_succ[1]:
            return self._SR2
        else:
            return 1

    def __neighbors(self, s_vertex: Vertex) -> List[Vertex]:
        """
        Returns list of vertex's neighbors on the grid, excluding those with
        infinite value.
        """
        result = []
        for i in range(-1, 2):
            for j in range(-1, 2):
                if i == j == 0:
                    continue
                x = s_vertex[0] + i
                y = s_vertex[1] + j
                if (
                    0 <= x < self.s_grid.shape[0]
                    and 0 <= y < self.s_grid.shape[1]
                    and self.s_grid[y, x] != math.inf
                ):
                    result.append((x, y))
        return result

    def __successors(self, s_vertex: Vertex) -> List[Vertex]:
        """
        Returns a list of successors. All neighbors in the grid world.
        Needs verification.
        """
        return self.__neighbors(s_vertex)
        # neighbors = self.neighbors(s)
        # return [e for e in neighbors if e not in self.predecessors()]

    def __predecessors(self, s_vertex: Vertex) -> List[Vertex]:
        """
        Returns a list of predecessors. All neighbors in the grid world.
        Needs verification.
        """
        return self.__neighbors(s_vertex)

    def __calc_rhs(self, s_vertex: Vertex) -> float:
        """
        Calculate right-hand-side value for a vertex.
        """
        values = [self.__get_g(s_prim) + self.__c_cost(s_vertex, s_prim)
                  for s_prim in self.__successors(s_vertex)]
        return min(values)

    def __get_rhs(self, s_vertex: Vertex) -> float:
        """
        Get vertex's previously calculated right-hand-side value.
        """
        return self.rhs_values[s_vertex[1], s_vertex[0]]

    def __get_g(self, s_vertex: Vertex) -> float:
        """
        Get vertex's previously calculated g value.
        """
        return self.g_values[s_vertex[1], s_vertex[0]]

    def __get_h(self, s_vertex: Vertex, s_prim: Vertex) -> float:
        """
        Heuristic function. Octile distance for 8-directional, non-uniform cost
        grid world.
        """
        if s_vertex == s_prim:
            return 0
        d_x = abs(s_vertex[0] - s_prim[0])
        d_y = abs(s_vertex[1] - s_prim[1])
        return (d_x + d_y) + (self._SR2 - 2) * min(d_x, d_y)

    def __calc_key(self, s_vertex: Vertex) -> Key:
        """
        Calculate vertex's key value for the priority queue.
        """
        key1 = min(self.__get_g(s_vertex), self.__get_rhs(s_vertex)) + \
            self.__get_h(self.s_start, s_vertex) + self.km
        key2 = min(self.__get_g(s_vertex), self.__get_rhs(s_vertex))
        return (key1, key2)

    def __get_shortest_path(self) -> List[Vertex]:
        """
        Get the correct path after all calculations are done.
        """
        path = []
        if self.__get_rhs(self.s_start) != math.inf:
            s_vertex = self.s_start
            while s_vertex != self.s_goal:
                path.append(s_vertex)
                # for each predecessor calculate g(p) + c(p, s) and select one with lowest result
                _, s_vertex = min([(self.__get_g(p) + self.__c_cost(s_vertex, p), p)
                                   for p in self.__successors(s_vertex)])
            path.append(self.s_goal)
        return path



    def get_visualization(self) -> Optional[Column]:
        if not self.visualize:
            return None

        plot = figure(outer_width=500,
                      outer_height=500)

        data_src = ColumnDataSource(data={
            "grid_img": [self.s_grid],
            "g_img": [self.g_values_cache[0]],
            "rhs_img": [self.rhs_values_cache[0]],
        })

        position_data_src = ColumnDataSource(data={
            "x": [self.visual_result[0][0]],
            "y": [self.visual_result[1][0]]
        })

        result_data_src = ColumnDataSource(data={
            "x": self.visual_result[0],
            "y": self.visual_result[1]
        })

        palette_grid = [RGB(0, 0, 0, 0)] + [RGB(x, 0, 0, 1)
                                            for x in range(1, 255)]
        palette_g = [RGB(0, x, 0, 1)
                     for x in range(255, 1, -1)] + [RGB(0, 0, 0, 0)]
        palette_rhs = [RGB(0, 0, x, 0.5)
                       for x in range(255, 1, -1)] + [RGB(0, 0, 0, 0)]

        grid_w = self.s_grid.shape[0]
        grid_h = self.s_grid.shape[1]
        grid_renderer = plot.image(image='grid_img', x=0, y=0, dw=grid_w, dh=grid_h,
                   source=data_src, palette=palette_grid, name='grid_plot')
        g_renderer = plot.image(image='g_img', x=0, y=0, dw=grid_w, dh=grid_h,
                   source=data_src, palette=palette_g, name='g_plot')
        rhs_renderer = plot.image(image='rhs_img', x=0, y=0, dw=grid_w, dh=grid_h,
                   source=data_src, palette=palette_rhs, name='rhs_plot')
        plot.line(x="x", y="y", source=result_data_src, line_color='black', line_width=3, legend_label="Shortest path")
        plot.line(x="x", y="y", source=result_data_src, line_color='white', legend_label="Shortest path")
        plot.circle_dot(x="x", y="y", source=position_data_src, legend_label="Current position", size=10)

        hovertool = HoverTool(
            tooltips=[
                ("x, y", "$x{0.}, $y{0.}"),
                ("grid value", "@grid_img{%.3f}"),
                ("g value", "@g_img{%.3f}"),
                ("rhs value", "@rhs_img{%.3f}")],
            formatters={
                "@grid_img": "printf",
                "@g_img": "printf",
                "@rhs_img": "printf",
            },
            renderers=[grid_renderer, g_renderer, rhs_renderer]
        )
        plot.add_tools(hovertool)

        buttons = RadioButtonGroup(labels=["Change Start Point", "Change End Point", "Add Barrier", "Delete Barrier"])
        slider = Slider(start=0, end=len(self.g_values_cache)-1, value=0, step=1, title="Iterations of search algorithm")
        position_slider = Slider(start=0, end=len(self.visual_result[0])-1, value=0, step=1, title="Steps along found path")

        def tap_handler(event: Event) -> None:
            if (isinstance(event, Tap) and event is not None and event.x is not None and event.y is not None):
                click_x = math.floor(event.x)
                click_y = math.floor(event.y)
                print(click_x, click_y)
                if self.edit_mode is None or self.edit_mode > 3: return

                new_start = self.s_start
                new_goal = self.s_goal
                if self.edit_mode == 0: 
                    new_start = (click_x, click_y)
                elif self.edit_mode == 1:
                    new_goal = (click_x, click_y)
                else:
                    self.s_grid[click_y, click_x] = math.inf if self.edit_mode == 2 else 0

                self.g_values_cache = []
                self.rhs_values_cache = []
                self.compute(new_start, new_goal)
                data_src.data = dict(
                    grid_img=[self.s_grid],
                    g_img=[self.g_values_cache[0]],
                    rhs_img=[self.rhs_values_cache[0]])
                position_data_src.data = dict(
                    x=[self.visual_result[0][0]],
                    y=[self.visual_result[1][0]]
                )
                result_data_src.data = dict(
                    x=self.visual_result[0],
                    y=self.visual_result[1]
                )
                slider.end=len(self.g_values_cache)-1
                slider.value=0
                position_slider.end=len(self.visual_result[0])-1
                position_slider.value=0

        plot.on_event(Tap, tap_handler)

        def slider_update(_attr, _old, new):
            data_src.data = dict(
                grid_img=[self.s_grid],
                g_img=[self.g_values_cache[new]],
                rhs_img=[self.rhs_values_cache[new]])
        slider.on_change('value', slider_update)

        def mode_changed(attr, old, new) -> None: 
            self.edit_mode = new
        buttons.on_change("active", mode_changed)

        def change_position(attr, old, new):
            position_data_src.data = dict(
                x=[self.visual_result[0][new]],
                y=[self.visual_result[1][new]]
            )
        position_slider.on_change('value', change_position)

        note = Div(
            text='''
This is a simple demonstration of a basic implementation of D* Lite shortest 
path algorithm, implemented from scratch, based on following research paper 
<a href="https://www.cs.cmu.edu/~maxim/files/dlite_tro05.pdf">https://www.cs.cmu.edu/~maxim/files/dlite_tro05.pdf</a>.

This demo allows for a basic interactions with the search algorithm and returns
visual data of it's inner workings for easier validation. 

User can modify the algorithm input by first selecting from available editing modes:
<ul>
<li>Changing the start point of an algorithm</li>
<li>Changing the end point of an algorithm</li>
<li>Adding a barrier element</li>
<li>Removing a barrier element</li>
</ul>

After selecting a mode modification can be made by clicking a point on the map diagram.
<br>
Below the mode buttons there are 2 sliders. First one moves an agent (in real 
life this would be a robot or a drone) along the found path. This is done in 
preparation for implementing dynamic changes to the environment, without 
recalculating the whole path from the beggining (which this algorithm supports).
The second one allows to move through all of the iterations of the search 
algorithm, and display g and rhs values for each node. 
<br>
Note: D* Lite searches from end to start, but returns path from start to end.
<br>
Explanation of the colors on the diagram:
<ul>
<li>Red - represents a barrier, that blocks the passage. Note: 2 neighboring diagonal pixels without bariers form a valid path since pixels are actually just a visual representation for a node on a graph.</li>
<li>Light blue - Algorithm has visited this node and assigned it an rhs value.</li>
<li>Blue-green - Algorithm has visited this node and assigned it an rhs and g values.</li>
</ul>


''')

        return layout(children=[plot, buttons, position_slider, slider, note]) # type: ignore

def get_visual_result(result: List[Vertex]):
    if(len(result) == 0): return [[0], [0]]
    result_offset = np.full((len(result), 2), 0.5)
    return list(zip(*np.add(result, result_offset)))